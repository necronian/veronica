#!/bin/bash

SRCDEST=/usr/src
PKGDEST=/usr/src/xstow
INSTALLDIR=/usr
INSTALLROOT=/
LOGDIR=/usr/src/logdir
STOWCONF=/etc/xstow.ini
RECIPELOCATION=./recipies

TIMEFORMAT=$'real\t%R\nuser\t%U\nsys\t%S\ncpu\t%P'

STAGES=( "p_setup" "p_fetch" "p_extract" "p_patch" "p_configure" "p_build" "p_test" "p_install" "p_link")

if [ -f /etc/veronica.conf ]; then
    source /etc/veronica.conf
fi

p_configure() {
    ./configure --prefix="${INSTALLDIR}"
}

p_build() {
    make
}

p_test() {
    make check
}

p_install() {
    make DESTDIR="${PKGDEST}/${PKGFULL}" install
}

p_link() {
    xstow-static -v 1 -F "${STOWCONF}" -d "${PKGDEST}" -t "${INSTALLROOT}" "${PKGFULL}"
}

p_setup() {
    mkdir -pv "${SRCDEST}/${PKGFULL}"
    mkdir -pv "${PKGDEST}/${PKGFULL}"
    cd "${SRCDEST}/${PKGFULL}"
}

# Fetch all the files define in the source array
p_fetch() {
    for SRCFILE in ${SOURCE[@]}
    do
	local PROTOCOL="${SRCFILE%%://*}"
	case $PROTOCOL in
	    http|ftp)
                wget ${SRCFILE##http://}
	    ;;
            file)
	        cp -v ${SRCFILE##file://} .
	    ;;
	    git)
	        git clone ${SRCFILE$$file://}
	    ;;
	esac
    done
}

p_extract() {
    for SRCFILE in ${SOURCE[@]}
    do
	local TYPE=$(file -bizL "${SRCFILE##*/}")
	case $TYPE in
    	    *application/x-tar*)
		tar -xvf "${SRCFILE##*/}"
	esac
    done
    cd "${PKGNAME}-${PKGVER}"
}

run_function() {

    local FNAME=$1

    #If the function exists run it
    declare -f ${FNAME} > /dev/null
    if (( ! $?)) ; then
        $FNAME
    fi

}

run_stage() {
    local SNAME=$1

    run_function "pre_${SNAME}"
    run_function "${SNAME}"
    run_function "post_${SNAME}"
}

execute_stages() {
    for STAGE in ${STAGES[@]}
    do
	run_stage ${STAGE}
    done
}

#A command that searches through the recipes in the directory 
#Takes two arguments the recipe we are serching for and the recipe directory
#Returns 0/1 on success/error
#The recipe name/version number/and release number will be placed into the 
#global variables PKGNAME/PKGVERSION/PKGREL respectively
find_recipe() {
    local LVER=""
    local LREL=""

    local RLOCATION=$2

    #Loop through all the recipies in the recipe dir that match the input 
    for file in `ls ${RLOCATION} | grep $1`
    do
	#Extract the relevant package information from the matched file
	PKGNAME=`echo $file| sed 's/^\(.*\)-[0-9].*/\1/'` 
	PKGVERSION=`echo $file| sed "s/${PKGNAME}-\(.*\)_.*/\1/"`
	PKGREL=`echo $file| sed "s/${PKGNAME}-${PKGVERSION}_//"`

	#Populate LVER and LREL for later comparison
	#If PKGNAME != the package we are looking for ignore it to avoid a bug
	#
	if [[ $LVER == "" && $1 = $PKGNAME ]]; then
	    LVER=$PKGVERSION
	fi
	if [[ $LREL == "" && $1 = $PKGNAME ]]; then
	    LREL=$PKGREL
	fi

	#Check if the current file is a greater version than the last file matched
	if [[ $1 = $PKGNAME ]]; then
	    if [[ ($PKGVERSION == $LVER) ]]; then
		if [[ $PKGREL > $LREL ]]; then
	    	    LVER=$PKGVERSION
		    LREL=$PKGREL
		fi
		else 
		if [[ ($PKGVERSION > $LVER) ]] ; then 
		    LVER=$PKGVERSION
		    LREL=$PKGREL
		fi
	    fi

	fi
    done

    #Now that we have the recipe we want make sure it exists and retun sucess/error
    echo ${RLOCATION}
    echo ${PKGNAME}
    echo ${LVER}
    echo ${LREL}
    echo ${RLOCATION}/${PKGNAME}-${LVER}_${LREL}
    if [ -f "${RLOCATION}/${PKGNAME}-${LVER}_${LREL}" ]
    then
	PKGNAME=${PKGNAME}
	PKGVER=${LVER}
	PKGREL=${LREL}
	PKGFULL="${PKGNAME}-${PKGVER}_${PKGREL}"
	return 0
    fi
    return 1
}

mkdir -pv "${LOGDIR}"
find_recipe $1 "${RECIPELOCATION}"
if [ $? -eq 0 ]; then
    if [[ $2 == "R" ]] ; then
	xstow-static -D -v 1 -F "${STOWCONF}" -d "${PKGDEST}" -t "${INSTALLROOT}" "${PKGFULL}"
    else
	source "${RECIPELOCATION}/${PKGFULL}"
	L="${LOGDIR}/$(date --rfc-333=seconds)-${PKGFULL}"
	((time (( execute_stages 3>&1 1>&2 2>&3 ) | tee "${L}-error.log" ) 2>&1| tee "${L}-all.log") 2> "${L}-time.log")
    fi
else
    echo "ERROR NO RECIPE"
    exit 1
fi
